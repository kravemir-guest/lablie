# Summary

[lablie](./index.md)

- [Installation](installation.md)
- [CLI Usage](cli/index.md)
    - [tile command](cli/command/tile.md)
    - [instance command](cli/command/instance.md)
    - [project command](cli/command/project.md)
- [CLI Examples](cli-examples/index.md)
    - [tile label](cli-examples/01-tile-label/index.md)
    - [tile label with instancing](cli-examples/02-tile-label-with-instancing/index.md)
    - [project example](cli-examples/11-example-project/index.md)
    - [batch instancing using shell](cli-examples/91-batch-instacing-using-shell/index.md)
    - [batch instancing using makefile](cli-examples/92-batch-instacing-using-makefile/index.md)
- [For Developers](developers/index.md)
- [CI / CD](ci-cd.md)
