# lablie

`lablie` is designed to be simply usable tool to prepare documents with labels for printing.

The main features are:

* [instance][instance-command] SVG template with provided data,
* [tile][tile-command] SVG to meet specified print-paper layout,
* [project][project-command] support for complex usages.

[instance-command]: cli/command/instance.md
[tile-command]: cli/command/tile.md
[project-command]: cli/command/project.md

## License

The project is licensed under [Apache License, Version 2.0, January 2004][apache-license-v2].

[apache-license-v2]: https://www.apache.org/licenses/LICENSE-2.0
